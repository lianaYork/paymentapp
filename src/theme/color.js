export default {
  primaryColor: '#4c2a86',
  darkPrimaryColor: '#7B1FA2',
  lightPrimaryColor: '#E1BEE7',
  textIcons: '#FFFFFF',
  accentIcons: '#4c2a86',
  primaryText: '#212121',
  secondaryText: '#757575',
  dividerColor: '#D1D1D1',
  errorColor: '#E4202D',
  successColor: '#1AB385',
  starColor: '#FEBF35'
}

import React from 'react'
import {
  Image,
  StyleSheet,
  TouchableWithoutFeedback
} from 'react-native'
import {
  Container,
  Text,
  View,
  Icon
} from 'native-base'
import {
  createStackNavigator,
  createBottomTabNavigator,
  createSwitchNavigator
} from 'react-navigation'
import TabBarItem from 'components/TabBarItem'
import Home from 'screens/Home'
import DetailProduct from 'screens/DetailProduct'
import ListProduct from 'screens/ListProduct'
import Scan from 'screens/Scan'
import Code from 'screens/Code'
import Transfer from 'screens/Transfer'
import Search from 'screens/Search'
import SignIn from 'screens/SignIn'
import SignUp from 'screens/SignUp'
import AuthLoadingScreen from 'screens/AuthLoadingScreen'
import color from 'theme/color'
import { APPNAME } from 'utils/config'

const styles = StyleSheet.create({
  header: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 10,
    paddingBottom: 2
  },
  headerText: {
    fontSize: 20,
    color: color.textIcons
  }
})

const headerOption = ({ headerTitle }) => {
  let headerTitleStyle = {
    color: color.textIcons,
    fontSize: 18,
    flex: 1
  }
  let headerTintColor = color.textIcons
  let headerStyle = {
    backgroundColor: color.primaryColor,
    backgroundOpacity: 0.7
  }
  let headerBackTitle = ''
  return {
    headerTitle,
    headerTitleStyle,
    headerStyle,
    headerTintColor,
    headerBackTitle
  }
}

const BottomTabNavigatorConfig = {
  initialRouteName: 'Home',
  backBehavior: 'none',
  tabBarPosition: 'bottom',
  swipeEnabled: false,
  mode: 'card',
  tabBarOptions: {
    showIcon: true,
    activeTintColor: color.primaryColor,
    inactiveTintColor: color.secondaryText,
    style: {
      borderTopWidth: 1,
      elevation: 6,
      backgroundColor: color.textIcons,
      height: 60
    },
    labelStyle: {
      marginTop: -4,
      fontSize: 12
    },
    indicatorStyle: {
      height: 0
    }
  }
}

const HomeScreen = createStackNavigator({
  HomeScreen: {
    screen: Home,
    navigationOptions: headerOption({
      headerTitle: (
        <View style={styles.header}>
          <Image
            style={{
              width: 36,
              height: 36
            }}
            resizeMode="contain"
            source={require('../assets/app/logo.png')}
          />
          <Text style={styles.headerText}>{APPNAME}</Text>
        </View>
      )
    })
  }
})

const Main = createBottomTabNavigator({
  Home: {
    screen: HomeScreen,
    navigationOptions: () => {
      return ({
        tabBarLabel: 'Home',
        tabBarIcon: ({ focused, tintColor }) => (
          <TabBarItem
            tintColor={tintColor}
            focused={focused}
            iconName="home"
            typeIcon="Foundation"
          />
        )
      })
    }
  },
  Order: {
    screen: createStackNavigator({
      OrderScreen: {
        screen: HomeScreen,
        navigationOptions: ({ navigation }) => headerOption({
          headerTitle: (
            <TouchableWithoutFeedback onPress={() => navigation.navigate('Search')}>
              <View style={styles.header}>
                <Icon
                  style={{
                    marginRight: 16,
                    fontSize: 20,
                    color: color.textIcons
                  }}
                  name="search"
                  type="FontAwesome"
                />
                <Text style={styles.headerText}>{`Cari di ${APPNAME}`}</Text>
              </View>
            </TouchableWithoutFeedback>
          )
        })
      }
    }),
    navigationOptions: () => ({
      tabBarLabel: 'Deals',
      tabBarIcon: ({ focused, tintColor }) => (
        <TabBarItem
          focused={focused}
          tintColor={tintColor}
          iconName="shopping-bag"
          typeIcon="FontAwesome"
        />
      )
    })
  },
  Finance: {
    screen: createStackNavigator({
      OrderScreen: {
        screen: HomeScreen,
        navigationOptions: ({ navigation }) => headerOption({
          headerTitle: (
            <TouchableWithoutFeedback onPress={() => navigation.navigate('Search')}>
              <View style={styles.header}>
                <Icon
                  style={{
                    marginRight: 16,
                    fontSize: 20,
                    color: color.textIcons
                  }}
                  name="search"
                  type="FontAwesome"
                />
                <Text style={styles.headerText}>{`Cari di ${APPNAME}`}</Text>
              </View>
            </TouchableWithoutFeedback>
          )
        })
      }
    }),
    navigationOptions: () => ({
      tabBarLabel: 'Finance',
      tabBarIcon: ({ focused, tintColor }) => (
        <TabBarItem
          focused={focused}
          tintColor={tintColor}
          iconName="notebook"
          typeIcon="MaterialCommunityIcons"
        />
      )
    })
  },
  Favourite: {
    screen: createStackNavigator({
      FavouriteScreen: {
        screen: HomeScreen,
        navigationOptions: headerOption({
          headerTitle: (
            <View style={styles.header}>
              <Text style={styles.headerText}>Wallet</Text>
            </View>
          )
        })
      }
    }),
    navigationOptions: () => ({
      tabBarLabel: 'Wallet',
      tabBarIcon: ({ focused, tintColor }) => (
        <TabBarItem
          focused={focused}
          tintColor={tintColor}
          iconName="wallet"
          typeIcon="MaterialCommunityIcons"
        />
      )
    })
  },
  Profile: {
    screen: createStackNavigator({
      FavouriteScreen: {
        screen: HomeScreen,
        navigationOptions: headerOption({
          headerTitle: (
            <View style={styles.header}>
              <Text style={styles.headerText}>Informasi</Text>
            </View>
          )
        })
      }
    }),
    navigationOptions: () => ({
      tabBarLabel: 'History',
      tabBarIcon: ({ focused, tintColor }) => (
        <TabBarItem
          focused={focused}
          tintColor={tintColor}
          iconName="bar-graph"
          typeIcon="Entypo"
        />
      )
    })
  }
}, BottomTabNavigatorConfig)

const MainAuth = createBottomTabNavigator({
  Home: {
    screen: SignIn,
    navigationOptions: () => {
      return ({
        tabBarLabel: 'Home',
        tabBarIcon: ({ focused, tintColor }) => (
          <TabBarItem
            tintColor={tintColor}
            focused={focused}
            iconName="home"
            typeIcon="Foundation"
          />
        )
      })
    }
  },
  Order: {
    screen: SignIn,
    navigationOptions: () => ({
      tabBarLabel: 'Deals',
      tabBarIcon: ({ focused, tintColor }) => (
        <TabBarItem
          focused={focused}
          tintColor={tintColor}
          iconName={focused ? 'home' : 'home-outline'}
          typeIcon={focused ? 'Foundation' : 'MaterialCommunityIcons'}
        />
      )
    })
  },
  Finance: {
    screen: SignIn,
    navigationOptions: () => ({
      tabBarLabel: 'Finance',
      tabBarIcon: ({ focused, tintColor }) => (
        <TabBarItem
          focused={focused}
          tintColor={tintColor}
          iconName="notebook"
          typeIcon="MaterialCommunityIcons"
        />
      )
    })
  },
  Favourite: {
    screen: SignIn,
    navigationOptions: () => ({
      tabBarLabel: 'Wallet',
      tabBarIcon: ({ focused, tintColor }) => (
        <TabBarItem
          focused={focused}
          tintColor={tintColor}
          iconName="wallet"
          typeIcon="MaterialCommunityIcons"
        />
      )
    })
  },
  Profile: {
    screen: SignIn,
    navigationOptions: () => ({
      tabBarLabel: 'Profile',
      tabBarIcon: ({ focused, tintColor }) => (
        <TabBarItem
          focused={focused}
          tintColor={tintColor}
          iconName={focused ? 'user' : 'user-o'}
          typeIcon="FontAwesome"
        />
      )
    })
  }
}, BottomTabNavigatorConfig)

const LoginStack = createStackNavigator(
  {
    MainAuth: {
      screen: MainAuth
    },
    SignIn: {
      screen: SignIn
    },
    SignUp: {
      screen: SignUp
    },
    Search: {
      screen: Search
    },
    ListProduct: {
      screen: ListProduct
    },
    DetailProduct: {
      screen: DetailProduct
    }
  }, {
    initialRouteName: 'MainAuth',
    navigationOptions: { header: null }
  }
)

const RequireAuth = createStackNavigator(
  {
    Main: {
      screen: Main
    },
    Search: {
      screen: Search
    },
    Transfer: {
      screen: createStackNavigator({
        TransferScreen: {
          screen: Transfer
        }
      })
    },
    Scan: {
      screen: createStackNavigator({
        ScanScreen: {
          screen: Scan
        }
      })
    },
    Code: {
      screen: createStackNavigator({
        CodeScreen: {
          screen: Code
        }
      })
    },
    ListProduct: {
      screen: ListProduct
    },
    DetailProduct: {
      screen: DetailProduct
    }
  }, {
    initialRouteName: 'Main',
    navigationOptions: { header: null }
  }
)

const AppNavigator = createSwitchNavigator(
  {
    AuthLoading: AuthLoadingScreen, // Utk check login
    App: RequireAuth, // Utk required login
    Auth: LoginStack // Utk not required
  }, {
    initialRouteName: 'AuthLoading'
  }
)

export default class Routes extends React.Component {
  render () {
    return (
      <Container>
        <AppNavigator />
      </Container>
    )
  }
}

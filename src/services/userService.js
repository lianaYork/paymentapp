import request from 'utils/request'

const myUser = async (data) => {
  return request({
    url: '/auth/me',
    data,
    auth: true,
    method: 'get'
  })
}

export {
  myUser
}

import request from 'utils/request'

const login = async (data) => {
  return request({
    url: '/auth/login',
    data,
    method: 'post'
  })
}

export {
  login
}

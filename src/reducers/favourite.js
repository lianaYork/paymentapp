import { IMAGEURL } from 'utils/config'
import {
  FETCH_HOME,
  RECEIVE_HOME
} from '../actions/types'

const initialState = {
  loadingFavourite: false,
  errorMessage: '',
  listFavourite: [
    {
      id: 1,
      image: `${IMAGEURL}/image1.jpg`,
      name: 'Nama Wisata',
      province: 'Provinsi',
      city: 'Kota',
      price: 200000,
      priceText: 'Rp 200.000',
      priceDiscount: 350000,
      priceDiscountText: 'Rp 350.000',
      status: 0
    },
    {
      id: 2,
      image: `${IMAGEURL}/image1.jpg`,
      name: 'Nama Wisata',
      province: 'Provinsi',
      city: 'Kota',
      price: 200000,
      priceText: 'Rp 200.000',
      priceDiscount: 350000,
      priceDiscountText: 'Rp 350.000',
      status: 1
    },
    {
      id: 3,
      image: `${IMAGEURL}/image1.jpg`,
      name: 'Nama Wisata',
      province: 'Provinsi',
      city: 'Kota',
      price: 200000,
      priceText: 'Rp 200.000',
      priceDiscount: null,
      priceDiscountText: null,
      status: 1
    },
    {
      id: 4,
      image: `${IMAGEURL}/image1.jpg`,
      name: 'Nama Wisata',
      province: 'Provinsi',
      city: 'Kota',
      price: 200000,
      priceText: 'Rp 200.000',
      priceDiscount: 350000,
      priceDiscountText: 'Rp 350.000',
      status: 1
    }
  ]
}

export default (state = initialState, action = {}) => {
  switch (action.type) {
    case FETCH_HOME: {
      return Object.assign({}, state, {
        loadingHome: true
      })
    }
    case RECEIVE_HOME: {
      return Object.assign({}, state, {
        loadingHome: false
      })
    }
    default:
      return state
  }
}

const APIHOST = '192.168.100.14'
const APIPORT = '8080'
const APIVERSION = '/api'

module.exports = {
  APPNAME: 'Payment',
  IMAGEURL: 'http://192.168.100.14:3100',
  APIURL: `http://${APIHOST}:${APIPORT}${APIVERSION}`,
  apiAuth: '/auth',
  apiForgotPsswd: '/auth/forgot-password',
  apiDataForgotPsswd: '/auth/view-data-forgot',
  apiUser: '/users',
  apiAddress: '/address',
  apiTrade: '/trade',
  apiUserView: '/users/view-profile',
  apiUserEdit: '/users/edit-profile',
  apiRegional: '/tracking/get',
  apiTradeView: '/trade/view',
  apiTradeCheckDomanin: '/trade/check-domain',
  apiTradeStepOne: '/trade/create?step=1',
  apiTradeStepTwo: '/trade/create?step=2&userTradeId=',
  apiTradeStepThree: '/trade/create?step=3&userTradeId=',
  apiTradeStepFour: '/trade/create?step=4&userTradeId=',
  apiCheckPsswd: '/check-password'
}

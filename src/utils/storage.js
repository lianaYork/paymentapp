import {
  AsyncStorage
} from 'react-native'

const getUserToken = async () => {
  let data = await AsyncStorage.getItem('token')
  return data
}

const get = async (key) => {
  let data = []
  try {
    data = await AsyncStorage.getItem(key)
    return data
  } catch (error) {
    console.log('Storage Failed:', error)
  }
}

const set = async (key, data) => {
  try {
    return AsyncStorage.setItem(key, data)
  } catch (error) {
    console.log('Storage Failed:', error)
  }
}

const remove = async (key) => {
  const removeProgress = await AsyncStorage.removeItem(key)
  return removeProgress
}

export {
  getUserToken,
  get,
  set,
  remove
}

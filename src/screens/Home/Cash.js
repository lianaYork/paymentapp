import React, { PureComponent } from 'react'
import {
  StyleSheet
} from 'react-native'
import {
  View,
  Text,
  Item,
  Button
} from 'native-base'
import color from 'theme/color'

const styles = StyleSheet.create({
  cashLeft: {
    padding: 10,
    justifyContent: 'center',
    height: 130,
    backgroundColor: color.primaryColor
  },
  item: {
    borderBottomWidth: 0
  },
  miniTextCurrency: {
    fontSize: 12,
    lineHeight: 18,
    color: color.starColor
  },
  textCurrency: {
    paddingHorizontal: 5,
    fontSize: 24,
    lineHeight: 30,
    color: color.starColor
  },
  smallTextCurrency: {
    paddingHorizontal: 5,
    fontSize: 18,
    lineHeight: 30,
    color: color.starColor
  },
  miniText: {
    fontSize: 14,
    color: color.textIcons
  },
  button: {
    position: 'absolute',
    right: 10,
    borderWidth: 0
  }
})

class Cash extends PureComponent {
  render () {
    return (
      <View style={styles.cashLeft}>
        <Text style={styles.miniText}>PAYMENT CASH</Text>
        <Item style={styles.item}>
          <Text style={styles.miniTextCurrency}>Rp</Text>
          <Text style={styles.textCurrency}>5.500</Text>
        </Item>
        <Item style={styles.item}>
          <Text style={styles.miniText}>PAYMENT POINTS</Text>
          <Text style={styles.smallTextCurrency}>20.000</Text>
        </Item>

        <Button success style={styles.button}>
          <Text style={styles.miniText}>
            TOP UP
          </Text>
        </Button>
      </View>
    )
  }
}

export default Cash

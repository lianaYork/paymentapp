import React, { PureComponent } from 'react'
// import PropTypes from 'prop-types'
import {
  StyleSheet,
  StatusBar
} from 'react-native'
import {
  Content
} from 'native-base'
import color from 'theme/color'

import Cash from './Cash'
import ActionButton from './ActionButton'

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column'
  },
  content: {
    padding: 10
  }
})

class Home extends PureComponent {
  render () {
    // const { navigation } = this.props
    return (
      <Content style={styles.container}>
        <StatusBar backgroundColor={color.primaryColor} />
        <Cash />
        <ActionButton />
      </Content>
    )
  }
}

// Home.propTypes = {
//   navigation: PropTypes.object.isRequired
// }

export default Home

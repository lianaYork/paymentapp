import React, { PureComponent } from 'react'
import {
  StyleSheet
} from 'react-native'
import {
  View,
  Text,
  Button,
  Icon
} from 'native-base'
import { withNavigation } from 'react-navigation'
import color from 'theme/color'

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    marginTop: -20,
    paddingHorizontal: 20
  },
  button: {
    paddingVertical: 10,
    flex: 1,
    height: 70,
    borderRadius: 0,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center'
  },
  buttonLeft: {
    borderRadius: 0,
    borderTopLeftRadius: 2,
    borderBottomLeftRadius: 2,
    paddingVertical: 10,
    flex: 1,
    height: 70,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center'
  },
  buttonRight: {
    borderRadius: 0,
    borderTopRightRadius: 2,
    borderBottomRightRadius: 2,
    paddingVertical: 10,
    flex: 1,
    height: 70,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center'
  },
  icon: {
    color: color.primaryColor
  },
  text: {
    fontSize: 14,
    color: color.primaryColor
  }
})

class ActionButton extends PureComponent {
  render () {
    const { navigation } = this.props
    return (
      <View style={styles.container}>
        <Button light style={styles.buttonLeft}>
          <Icon
            type="FontAwesome5"
            name="arrow-circle-right"
            style={styles.icon}
          />
          <Text uppercase={false} style={styles.text}>Transfer</Text>
        </Button>
        <Button
          light
          style={styles.button}
          onPress={() => navigation.navigate('Scan')}
        >
          <Icon
            type="AntDesign"
            name="scan1"
            style={styles.icon}
          />
          <Text uppercase={false} style={styles.text}>Scan</Text>
        </Button>
        <Button
          light
          style={styles.buttonRight}
          onPress={() => navigation.navigate('Code')}
        >
          <Icon
            type="FontAwesome"
            name="user-circle-o"
            style={styles.icon}
          />
          <Text uppercase={false} style={styles.text}>User ID</Text>
        </Button>
      </View>
    )
  }
}

export default withNavigation(ActionButton)

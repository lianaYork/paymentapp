import React, { PureComponent } from 'react'
import {
  StyleSheet,
  TouchableOpacity
} from 'react-native'
import {
  Header,
  Content,
  Item,
  Icon,
  Input
} from 'native-base'
import color from 'theme/color'
import { APPNAME } from 'utils/config'

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column'
  },
  header: {
    justifyContent: 'flex-start',
    alignItems: 'center'
  },
  item: {
    borderBottomWidth: 0
  },
  input: {
    color: color.textIcons,
    fontSize: 20
  }
})

class Search extends PureComponent {
  static navigationOptions = {
    header: null
  }

  state = {
    search: ''
  }

  render () {
    const { navigation } = this.props
    const { search } = this.state
    return (
      <Content style={styles.container}>
        <Header style={styles.header}>
          <Item onPress={this.handleGoToSearch} style={styles.item}>
            <TouchableOpacity onPress={() => navigation.goBack(null)}>
              <Icon name="ios-close" style={[styles.input, { fontSize: 40 }]} />
            </TouchableOpacity>
            <Input
              onChangeText={text => this.setState({ search: text })}
              onSubmitEditing={() => {
                if (search !== '' && !!search) {
                  navigation.navigate('ListProduct', {
                    q: search
                  })
                }
              }}
              autoFocus
              selectTextOnFocus
              placeholder={`Cari di ${APPNAME}`}
              placeholderTextColor={color.darkPrimaryColor}
              style={styles.input}
            />
            <TouchableOpacity
              onPress={() => {
                if (search !== '' && !!search) {
                  navigation.navigate('ListProduct', {
                    q: search
                  })
                }
              }}
            >
              <Icon name="search"
                type="FontAwesome"
                style={[styles.input, { fontSize: 24 }]}
              />
            </TouchableOpacity>
          </Item>
        </Header>
      </Content>
    )
  }
}

export default Search

import React, { PureComponent } from 'react'
import {
  StatusBar
} from 'react-native'
import {
  View,
  Text
} from 'native-base'
import color from 'theme/color'

class SignUp extends PureComponent {
  render () {
    return (
      <View>
        <StatusBar backgroundColor={color.primaryColor} />
        <Text>
          SignUp
        </Text>
      </View>
    )
  }
}

export default SignUp

const validate = ({
  username,
  password
}) => {
  const errors = {
    username: '',
    password: ''
  }

  errors.username = !username ? '*' : undefined
  errors.password = !password ? '*' : undefined

  return errors
}

export default validate

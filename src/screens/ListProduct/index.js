import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import {
  StyleSheet,
  StatusBar
} from 'react-native'
import {
  Content
} from 'native-base'
import color from 'theme/color'
import SearchBar from 'components/SearchBar'
import Product from './Product'

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column'
  }
})

class ListProduct extends PureComponent {
  state = {
    q: ''
  }

  componentWillMount () {
    const { navigation } = this.props
    const { q } = navigation.state.params
    if (q) {
      this.setState({ q })
    }
  }

  render () {
    const { navigation } = this.props
    const { q } = this.state
    return (
      <Content style={styles.container}>
        <StatusBar backgroundColor={color.primaryColor} />
        <SearchBar defaultValue={q} navigation={navigation} />
        <Product navigation={navigation} />
      </Content>
    )
  }
}

ListProduct.propTypes = {
  navigation: PropTypes.object.isRequired
}

export default ListProduct

import React from 'react'
import { connect } from 'react-redux'
import {
  ActivityIndicator,
  StatusBar,
  StyleSheet,
  View
} from 'react-native'
import {
  myUser
} from 'services/userService'
import { getUserToken } from 'utils/storage'
import color from 'theme/color'

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    justifyContent: 'center',
    alignItems: 'center'
  }
})

class AuthLoadingScreen extends React.Component {
  constructor (props) {
    super(props)
    this._bootstrapAsync()
  }

  // Fetch the token from storage then navigate to our appropriate place
  _bootstrapAsync = async () => {
    const userToken = await getUserToken()

    // This will switch to the App screen or Auth screen and this loading
    // screen will be unmounted and thrown away.\
    if (userToken && userToken !== '') {
      const userData = await myUser()
      if (userData.success) {
        return this.props.navigation.navigate('App')
      }
      return this.props.navigation.navigate('Auth')
    }
    this.props.navigation.navigate('Auth')
  };

  // Render any loading content that you like here
  render () {
    return (
      <View style={styles.container}>
        <StatusBar backgroundColor={color.primaryColor} />
        <ActivityIndicator />
      </View>
    )
  }
}

const mapStateToProps = state => ({
  isFetching: state.auth.isFetching
})

export default connect(mapStateToProps)(AuthLoadingScreen)

import React, { PureComponent } from 'react'
import {
  TouchableOpacity,
  StyleSheet
} from 'react-native'
import { RNCamera } from 'react-native-camera'
import {
  Icon,
  View,
  Container
} from 'native-base'
import color from 'theme/color'
import { BarcodeFinder } from './BarcodeFinder'

const styles = StyleSheet.create({
  preview: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center'
  },
  footer: {
    backgroundColor: color.textIcons,
    padding: 5,
    justifyContent: 'center',
    alignItems: 'center'
  },
  button: {
    borderWidth: 0,
    height: 80,
    width: '50%',
    flexDirection: 'column',
    backgroundColor: 'transparent',
    padding: 5,
    alignItems: 'center'
  },
  absolute: {
    backgroundColor: color.primaryText,
    opacity: 0.7,
    position: 'absolute',
    flex: 1,
    flexDirection: 'row',
    height: '100%',
    zIndex: 1
  },
  icon: {
    fontSize: 30,
    color: color.textIcons,
    marginVertical: 5
  },
  text: {
    color: color.textIcons,
    textAlign: 'center'
  }
})

class Scan extends PureComponent {
  static navigationOptions = ({ navigation }) => {
    const headerTitle = 'Pindai QR atau Barcode'
    let headerTitleStyle = {
      marginHorizontal: 0,
      color: color.textIcons,
      fontSize: 18,
      flex: 1
    }
    let headerTintColor = color.textIcons
    let headerStyle = {
      height: 60,
      backgroundColor: color.primaryColor,
      backgroundOpacity: 0.7
    }
    let headerBackTitle = ''
    return {
      headerTitle,
      headerTitleStyle,
      headerStyle,
      headerTintColor,
      headerBackTitle,
      backBehavior: 'initialRoute',
      headerLeft: (
        <TouchableOpacity
          style={{
            paddingHorizontal: 10
          }}
          onPress={() => { navigation.goBack(null) }}
        >
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center'
            }}
          >
            <Icon
              style={{
                color: color.textIcons
              }}
              name="ios-arrow-back"
            />
          </View>
        </TouchableOpacity>
      )
    }
  }

  takePicture = async () => { }

  render () {
    const { navigation } = this.props

    return (
      <Container>
        <RNCamera
          ref={(ref) => { this.camera = ref }}
          style={styles.preview}
          type={RNCamera.Constants.Type.back}
          flashMode={RNCamera.Constants.FlashMode.off}
          permissionDialogTitle="Permission to use camera"
          permissionDialogMessage="We need your permission to use your camera phone"
          onGoogleVisionBarcodesDetected={({ barcodes }) => {
            console.log('barcodes', barcodes)

            navigation.navigate('Transfer', {
              email: barcodes[0].data
            })
          }}
        />
        <BarcodeFinder width={280} height={220} borderColor="red" borderWidth={2} />
      </Container>
    )
  }
}

export default Scan

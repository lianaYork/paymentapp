import React, { PureComponent } from 'react'
import {
  TouchableOpacity,
  StyleSheet
} from 'react-native'
import {
  Container,
  Item,
  Content,
  Form,
  Input,
  View,
  Icon,
  Text,
  Button
} from 'native-base'

import color from 'theme/color'

const styles = StyleSheet.create({
  row: {
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: 50
  },
  bold: {
    textAlign: 'center',
    fontSize: 16,
    fontWeight: 'bold'
  },
  text: {
    textAlign: 'center',
    fontSize: 14
  },
  buttonContainer: {
    padding: 10,
    width: '100%'
  },
  button: {
    borderRadius: 5
  }
})

class Transfer extends PureComponent {
  static navigationOptions = ({ navigation }) => {
    const headerTitle = 'Masukkan nominal transfer'
    let headerTitleStyle = {
      marginHorizontal: 0,
      color: color.textIcons,
      fontSize: 18,
      flex: 1
    }
    let headerTintColor = color.textIcons
    let headerStyle = {
      height: 60,
      backgroundColor: color.primaryColor,
      backgroundOpacity: 0.7
    }
    let headerBackTitle = ''
    return {
      headerTitle,
      headerTitleStyle,
      headerStyle,
      headerTintColor,
      headerBackTitle,
      backBehavior: 'initialRoute',
      headerLeft: (
        <TouchableOpacity
          style={{
            paddingHorizontal: 10
          }}
          onPress={() => { navigation.goBack(null) }}
        >
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center'
            }}
          >
            <Icon
              style={{
                color: color.textIcons
              }}
              name="ios-arrow-back"
            />
          </View>
        </TouchableOpacity>
      )
    }
  }

  state = {
    transfer: null,
    loading: false
  }

  submitTransfer = async () => {
    this.setState({ loading: true })
    const { navigation } = this.props
    const { params } = navigation.state
    const { transfer } = this.state
    if (transfer && parseFloat(transfer) > 0) {
      console.log('transfer', transfer, params.email)
    }
    this.setState({ loading: false })
  }

  render () {
    const { loading } = this.state
    const { navigation } = this.props
    const { params } = navigation.state

    return (
      <Container>
        <Content>
          <View style={styles.row}>
            <Text style={styles.text}>Masukkan nominal yang akan ditransfer ke</Text>
            <Text style={styles.bold}>{params.email}</Text>
          </View>
          <Form>
            <Item>
              <Input
                keyboardType="numeric"
                maxLength={7}
                placeholder="Masukkan nominal transfer"
                onChangeText={(text) => {
                  if (text) {
                    text.replace(/[^\d]/g, '')
                  }
                  this.setState({ transfer: text })
                }}
              />
            </Item>
            <View style={styles.buttonContainer}>
              <Button
                block
                primary
                disabled={loading}
                style={styles.button}
                onPress={() => this.submitTransfer()}
              >
                <Text>Submit</Text>
              </Button>
            </View>
          </Form>
        </Content>
      </Container>
    )
  }
}

export default Transfer

import React, { PureComponent } from 'react'
import {
  TouchableOpacity,
  StyleSheet
} from 'react-native'
import {
  View,
  Text,
  Icon
} from 'native-base'
import QRCode from 'react-native-qrcode'
import { myUser } from 'services/userService'
import color from 'theme/color'

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  }
})

class Code extends PureComponent {
  static navigationOptions = ({ navigation }) => {
    const headerTitle = 'Pindai QR atau Barcode'
    let headerTitleStyle = {
      marginHorizontal: 0,
      color: color.textIcons,
      fontSize: 18,
      flex: 1
    }
    let headerTintColor = color.textIcons
    let headerStyle = {
      height: 60,
      backgroundColor: color.primaryColor,
      backgroundOpacity: 0.7
    }
    let headerBackTitle = ''
    return {
      headerTitle,
      headerTitleStyle,
      headerStyle,
      headerTintColor,
      headerBackTitle,
      backBehavior: 'initialRoute',
      headerLeft: (
        <TouchableOpacity
          style={{
            paddingHorizontal: 10
          }}
          onPress={() => { navigation.goBack(null) }}
        >
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center'
            }}
          >
            <Icon
              style={{
                color: color.textIcons
              }}
              name="ios-arrow-back"
            />
          </View>
        </TouchableOpacity>
      )
    }
  }

  state = {
    user: {}
  }

  componentWillMount = () => {
    this.renderUser()
  }

  renderUser = async () => {
    const userData = await myUser()
    if (userData.success) {
      this.setState({ user: userData.data.Data })
    }
  }

  render () {
    const { user } = this.state

    return (
      <View style={styles.container}>
        <QRCode
          value={user.email}
          size={200}
          bgColor="black"
          fgColor="white"
        />
        <Text
          style={{
            marginVertical: 10,
            textAlign: 'center'
          }}
        >
          {`${user.firstName} ${user.lastName}`}
        </Text>
      </View>
    )
  }
}

export default Code

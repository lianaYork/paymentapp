import React, { PureComponent } from 'react'
import {
  StyleSheet
} from 'react-native'
import {
  View
} from 'native-base'
import ImageSlider from 'components/ImageSlider'

const styles = StyleSheet.create({
  content: {
    flex: 1
  },
  segmentImage: {
    height: 150
  }
})

class Segment extends PureComponent {
  render () {
    const { product } = this.props
    return (
      <View style={styles.content}>
        <ImageSlider
          style={styles.stretch}
          arrow={false}
          height={200}
          downloadable={false}
          dataSource={[{ name: product.name, url: product.image }, { name: `${product.name}1`, url: product.image }]}
        />
      </View>
    )
  }
}

export default Segment

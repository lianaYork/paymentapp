import React, { PureComponent } from 'react'
import {
  TouchableOpacity,
  StatusBar
} from 'react-native'
import {
  View,
  Content,
  Icon,
  Text,
  Button
} from 'native-base'
import color from 'theme/color'
import globalStyle from 'theme/style'
import Header from './Header'
import Segment from './Segment'
import ProductTitle from './ProductTitle'
import ListDescription from './ListDescription'

class DetailProduct extends PureComponent {
  static navigationOptions = ({ navigation }) => {
    let headerTitle = 'Detail Produk'
    let headerTitleStyle = {
      marginHorizontal: 0,
      color: color.textIcons,
      fontSize: 18,
      flex: 1
    }
    let headerTintColor = color.textIcons
    let headerStyle = {
      height: 60,
      backgroundColor: color.primaryColor,
      backgroundOpacity: 0.7
    }
    let headerBackTitle = ''
    return {
      headerTitle,
      headerTitleStyle,
      headerStyle,
      headerTintColor,
      headerBackTitle,
      backBehavior: 'initialRoute',
      headerLeft: (
        <TouchableOpacity
          style={{
            paddingHorizontal: 10
          }}
          onPress={() => { navigation.goBack(null) }}
        >
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center'
            }}
          >
            <Icon
              style={{
                color: color.textIcons
              }}
              name="ios-arrow-back"
            />
          </View>
        </TouchableOpacity>
      ),
      headerRight: (
        <TouchableOpacity
          style={{
            paddingHorizontal: 10
          }}
        >
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center'
            }}
          >
            <Icon
              style={{
                color: color.textIcons
              }}
              name="share-google"
              type="EvilIcons"
            />
          </View>
        </TouchableOpacity>
      )
    }
  }

  state = {
    product: null
  }

  componentWillMount () {
    this.getProduct()
  }

  getProduct () {
    const { navigation } = this.props
    if (navigation.state.params) {
      const { product } = navigation.state.params
      this.setProduct(product)
    }
  }

  setProduct (product) {
    this.setState({ product })
  }

  render () {
    const { navigation } = this.props
    const { product } = this.state
    return (
      <Content>
        <StatusBar backgroundColor={color.primaryColor} />
        <Header navigation={navigation} />
        <Segment product={product} />
        <ProductTitle product={product} />
        <ListDescription product={product} />
        <View style={{ padding: 10 }}>
          <Button style={[globalStyle.buttonCenter, { backgroundColor: color.primaryColor }]}>
            <Text>Pemesanan</Text>
          </Button>
        </View>
      </Content>
    )
  }
}

export default DetailProduct

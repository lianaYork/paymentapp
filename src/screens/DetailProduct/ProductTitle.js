import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import {
  StyleSheet
} from 'react-native'
import {
  Text,
  View,
  Icon,
  Left,
  Right
} from 'native-base'
import color from 'theme/color'
import globalStyle from 'theme/style'

const styles = StyleSheet.create({
  content: {
    padding: 10,
    marginBottom: 10
  },
  location: {
    flexDirection: 'row'
  }
})

class ProductTitle extends PureComponent {
  render () {
    const { product } = this.props
    return (
      <View style={styles.content}>
        <View style={styles.location}>
          <Icon
            name="location-on"
            type="MaterialIcons"
            style={[
              globalStyle.h5,
              {
                width: 25,
                color: color.secondaryText,
                fontSize: 20
              }
            ]}
          />
          <Text style={globalStyle.h5}>{`${product.city} - ${product.province}`}</Text>
        </View>
        <Text style={globalStyle.h2}>{`${product.name}`}</Text>
        <View style={styles.location}>
          <Left>
            <Text style={globalStyle.text}>{`${product.priceText}`}</Text>
          </Left>
          <Right>
            <Text
              style={[
                globalStyle.text,
                {
                  fontSize: 12,
                  color: color.errorColor,
                  textDecorationLine: 'line-through',
                  textDecorationStyle: 'solid',
                  textAlign: 'right'
                }]}
            >
              {product.priceDiscountText}
            </Text>
          </Right>
        </View>
      </View>
    )
  }
}

ProductTitle.propTypes = {
  product: PropTypes.object
}

ProductTitle.defaultProps = {
  product: {}
}

export default ProductTitle
